# Use the official Red Hat 8 image as the base
FROM registry.access.redhat.com/ubi8/ubi

# Update the system and install development tools
# RUN yum -y update 
# RUN dnf -y groupinstall "Development Tools"
RUN yum -y install gcc gcc-c++ make
RUN yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
RUN yum -y install wxGTK3-devel
RUN yum -y install cmake
RUN yum -y install autoconf
RUN yum -y install automake
RUN yum -y install binutils
RUN yum -y install bison
RUN yum -y install flex
RUN yum -y install gcc
RUN yum -y install gcc-c++
RUN yum -y install gdb
RUN yum -y install glibc-devel
RUN yum -y install libtool
RUN yum -y install make
RUN yum -y install pkgconf
RUN yum -y install pkgconf-m4
RUN yum -y install pkgconf-pkg-config
RUN yum -y install redhat-rpm-config
RUN yum -y install rpm-build
RUN yum -y install rpm-sign
RUN yum -y install strace
RUN yum -y install git

# Install wxWidgets dependencies
RUN yum -y install libX11-devel libXt-devel libXinerama-devel libXcursor-devel libXxf86vm-devel libXi-devel libXrandr-devel libXfixes-devel libXrender-devel libXinerama-devel libXext-devel libGL-devel libGLU-devel

# Set the working directory
WORKDIR /wxWidgets_build

# Download the latest wxWidgets source code
RUN curl -L https://github.com/wxWidgets/wxWidgets/releases/download/v3.1.5/wxWidgets-3.1.5.tar.bz2 -o wxWidgets-3.1.5.tar.bz2 && \
    tar -xjf wxWidgets-3.1.5.tar.bz2

# Build and install wxWidgets
RUN cd wxWidgets-3.1.5 && \
    mkdir build && \
    cd build && \
    ../configure --with-gtk --enable-unicode && \
    make && \
    make install && \
    ldconfig

# Set the environment variable
ENV WXWIN /wxWidgets_build/wxWidgets-3.1.5

# Set the working directory for your application
WORKDIR /app

# Copy your application's source code into the container
COPY . /app

# Compile your application
RUN g++ -o my_app `wx-config --cxxflags --libs` main.cpp

# Set the command to run your application
CMD ["./my_app"]
